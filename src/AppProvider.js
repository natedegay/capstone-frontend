import React, { useState, createContext } from 'react';

export const AppContext = createContext();

export const AppProvider = (props) => {

	const [ authUser, setAuthUser] = useState([	])

	return(
		<AppContext.Provider value={[authUser,setAuthUser]}>
			{props.children}
		</AppContext.Provider>
	)
}
