import React, {useState, useEffect} from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import MainNav from './components/MainNav';
import Register from './components/Register';
import Login from './components/Login';
import Logout from './components/Logout';
import Rental from './components/Rental';
import RentalSingle from './components/RentalSingle';
import RentalCreate from './components/RentalCreate';
import RentalEdit from './components/RentalEdit';
import Header from './components/Header';
import Home from './components/Home';
import Footer from './components/Footer';
import RentalManage from './components/RentalManage';
import BookingManage from './components/BookingManage';

import Snackbar from '@material-ui/core/Snackbar';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';

import MuiAlert from '@material-ui/lab/Alert';

import { AppProvider } from './AppProvider';

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}


function App({message}) {

	const [ authUser, setAuthUser ] = useState({
        isAuth: false,
        _id: "",
        fullname:"",
        email:""
    });

    const handleClose = (event, reason) => {
        if (reason === 'clickaway') {
        return;
    }

    setOpen({
        open: false
    })
  }

    const [open, setOpen] = useState({
        open: false,
        severity:"",
        message:""
    });
	useEffect(()=>{
        let appState = localStorage["appState"];

        if(appState) {
            fetch("https://capstone-server-backend.herokuapp.com/users/profile",{
                headers: {
                    "Authorization" : `Bearer ${appState}`
                }
            })
            .then( response => response.json())
            .then( data => {
                if(data._id){
                    setAuthUser({
                        isAuth: true,
                        _id: data._id,
                        fullname: data.fullname,
                        email: data.email,
                        isAdmin: data.isAdmin
                    });
                
                }
                console.log(data)
            })
        }
    },[]);

    useEffect(()=>{
        if(authUser.isAuth){
            setOpen({
            open: true,
            severity:"success",
            message: "Logged in"
                })
        } else if(!authUser.isAuth) {
             setOpen({
            open: true,
            severity:"warning",
            message: "Logged Out"
                })
        }
        
    }, [authUser])

	return (
	    <AppProvider>
            <Router>
	    	
	    	<Header authUser={authUser}/>

	    	<Switch>

		    	<Route path="/register">
		    		<Register/>
		    	</Route>

		    	<Route path="/login">
		    		<Login authUser={authUser} setAuthUser={setAuthUser}/>
		    	</Route>

		    	<Route path="/logout">
		    		<Logout setAuthUser={setAuthUser}/>
		    	</Route>

		    	<Route exact path ="/">
		    		<Home authUser={authUser}/>
		    	</Route>

		    	<Route exact path="/rentals/:id">
                	<RentalSingle authUser={authUser} />
            	</Route>

            	<Route exact path="/create-rental">
            		<RentalCreate />
            	</Route>

            	<Route path="/rentals/:id/edit">
            		<RentalEdit />
            	</Route>

                <Route exact path="/rentals/users/manage">
                    <RentalManage />
                </Route>

                <Route exact path="/bookings/manage">
                    <BookingManage/>
                </Route>

	    	</Switch>

	    	<Footer/>
            <Snackbar 
                open={open.open} 
                autoHideDuration={6000} 
                onClose={handleClose}
                anchorOrigin={{
                    vertical: 'top',
                    horizontal: 'right',
                }}
            >
                <Alert onClose={handleClose} severity={open.severity} variant="filled">
                { open.open ? open.message : ""}
                </Alert>
            </Snackbar>
	       </Router>
        </AppProvider>
  	);
}

export default App;
