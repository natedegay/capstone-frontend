import React from 'react';
import RentalControl from './RentalControl'
import BookRental from './BookRental'
import '../css/Card.css';
import LocationOnIcon from '@material-ui/icons/LocationOn';
import LoyaltyIcon from '@material-ui/icons/Loyalty';
import CreditCardIcon from '@material-ui/icons/CreditCard';

const RentalCard = ({rental, withDescription, setDeletedRental}) => {

  return (
        <div className="card">
            <img src={`https://capstone-server-backend.herokuapp.com/${rental.image}`} alt="" className="card-img-top" />
            <div className="card__info">
            	<h1 className="card-title">{rental.title}</h1>
                <hr className="hr-text" data-content={rental.city.toUpperCase()} />
                <p className="description"> " {rental.description} " </p>
                <h4 className="card-text"> {(rental.dailyRate).toLocaleString('en-PH', {currency: 'PHP', style: 'currency'})} / night</h4>
            </div>
        </div>
  )
}

export default RentalCard;