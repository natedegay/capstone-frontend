import React from 'react';
import { Link } from 'react-router-dom';
import RentalControl from './RentalControl'
import BookRental from './BookRental'
import '../css/Card.css';
import LocationOnIcon from '@material-ui/icons/LocationOn';
import LoyaltyIcon from '@material-ui/icons/Loyalty';
import CreditCardIcon from '@material-ui/icons/CreditCard';


const ManageCard = ({rental, withDescription, setDeletedRental}) => {
  return (
     <div className="card">
            <Link to={`/rentals/${rental._id}/edit`} style={{ textDecoration: 'none'}}>
	            <img src={`https://capstone-server-backend.herokuapp.com/${rental.image}`} alt="" className="card-img-top" />
	            <div className="card__info">
	            	<h1 className="card-title">{rental.title}</h1>
	                <hr className="hr-text" data-content={rental.city.toUpperCase()} />
	                <p className="description"> " {rental.description} " </p>
	                <h4 className="card-text"> {(rental.dailyRate).toLocaleString('en-PH', {currency: 'PHP', style: 'currency'})} / night</h4>

	            </div>
	        </Link>
            <div className="card-footer">

    	        <RentalControl setDeletedRental={setDeletedRental} id={rental._id}/>
    	        
            </div>
        </div>
  )
}

export default ManageCard;