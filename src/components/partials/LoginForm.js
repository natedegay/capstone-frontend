import React, {useState} from 'react';
import { Redirect } from 'react-router-dom';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Paper from '@material-ui/core/Paper';
import Box from '@material-ui/core/Box';
import Grid from '@material-ui/core/Grid';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import { Link } from 'react-router-dom';

import Snackbar from '@material-ui/core/Snackbar';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';

import MuiAlert from '@material-ui/lab/Alert';

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}

const useStyles = makeStyles((theme) => ({
	  root: {
	    height: '100vh',
	  },
	  image: {
	    backgroundImage: 'url(https://images.unsplash.com/photo-1599056407101-7c557a4a0144?ixlib=rb-1.2.1&auto=format&fit=crop&w=700&q=80)',
	    backgroundRepeat: 'no-repeat',
	    backgroundColor:
	      theme.palette.type === 'light' ? theme.palette.grey[50] : theme.palette.grey[900],
	    backgroundSize: 'cover',
	    backgroundPosition: 'center',
	  },
	  paper: {
	    margin: theme.spacing(8, 4),
	    display: 'flex',
	    flexDirection: 'column',
	    alignItems: 'center',
	  },
	  avatar: {
	    margin: theme.spacing(1),
	    backgroundColor: theme.palette.secondary.main,
	  },
	  form: {
	    width: '100%', // Fix IE 11 issue.
	    marginTop: theme.spacing(1),
	  },
	  submit: {
	    margin: theme.spacing(3, 0, 2),
	  },
}));


const LoginForm = ({setAuthUser}) => {

	const classes = useStyles();

	const [open, setOpen] = useState({
		open: false,
		severity:"",
		message:""
	});

  const handleClose = (event, reason) => {
    	if (reason === 'clickaway') {
      	return;
    }

    setOpen({
    	open: false
    })
  }

	const [credentials, setCredentials ] = useState({
		email: "",
		password: ""
	});

	const [ isSuccess, setIsSuccess ] = useState(false);

	const [ isLoading, setIsLoading ] = useState(false);

	const [ name, setName ] = useState();

	if(isSuccess) {
		return <Redirect to={{
			pathname: '/',
			state: { fullname: name}
		}} />
	}

	const handlechange = e => {
		setCredentials({
			...credentials,
			[e.target.name] : e.target.value
		})
	};

	const handleSubmit = e => {
		e.preventDefault();
		setIsLoading(true);
		fetch(`https://capstone-server-backend.herokuapp.com/users/login`,{
			method: "post",
			body: JSON.stringify(credentials),
			headers: {
				"Content-Type" : "application/json"
			}
		})
		.then( response => {
			if(response.status !== 200)
                {
                	setOpen({
                		open: true,
						        severity:"error",
						        message:"Check Credentials"
                	})
            } 
            return response.json()
		})
		.then( data => {
			if(data.token) {
				localStorage["appState"] = data.token
				setAuthUser({
					isAuth: true,
					fullname: data.fullname,
					email: data.email,
					isAdmin: data.isAdmin
				})
				setName(data.fullname)
				setIsSuccess(true)
				setOpen({
                		open: true,
						severity:"success",
						message:"Logged in as " + data.fullname
                	})

			} else {
				setIsLoading(false)
			}

			console.log(data)
			setIsLoading(false)
		});

		
	}


	return (
	<Grid container component="main" className={classes.root}>
	 <div>
      <Snackbar 
      	open={open.open} 
      	autoHideDuration={6000} 
      	onClose={handleClose}
      	anchorOrigin={{
          vertical: 'top',
          horizontal: 'right',
        }}
      >
        <Alert onClose={handleClose} severity="error" variant="filled">
          { open.open ? open.message : ""}
        </Alert>
      </Snackbar>
    </div>
      <CssBaseline />
      <Grid item xs={false} sm={4} md={7} className={classes.image} />
      <Grid item xs={12} sm={8} md={5} component={Paper} elevation={6} square>
        <div className={classes.paper}>
          <Avatar className={classes.avatar}>
            <LockOutlinedIcon />
          </Avatar>
          <Typography component="h1" variant="h5">
            Log in
          </Typography>
          <form className={classes.form} noValidate onSubmit={handleSubmit}>
            <TextField
              variant="outlined"
              margin="normal"
              required
              fullWidth
              id="email"
              label="Email Address"
              name="email"
              autoComplete="email"
              autoFocus
              onChange={handlechange}
            />
            <TextField
              variant="outlined"
              margin="normal"
              required
              fullWidth
              name="password"
              label="Password"
              type="password"
              id="password"
              autoComplete="current-password"
              onChange={handlechange}
            />
            
            <Button
              type="submit"
              disabled={isLoading}
              fullWidth
              variant="contained"
              color="primary"
              className={classes.submit}
            >
            {	
            	isLoading ?
            	"Logging in..." :
              	"Log In"
            }
            </Button>
            <Grid container>
              <Grid item xs>
                <Link href="#" variant="body2">
                  Forgot password?
                </Link>
              </Grid>
              <Grid item>
                <Link to="/register" variant="body2">
                  {"Don't have an account? Sign Up"}
                </Link>
              </Grid>
            </Grid>
          </form>
        </div>
      </Grid>
    </Grid>
    	
	)
}

export default LoginForm;