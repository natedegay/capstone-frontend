import React, {useState} from 'react';
import Modal from "react-bootstrap/Modal";
import "bootstrap/dist/css/bootstrap.min.css";
import moment from 'moment';

import {getRangeOfDates} from './../../helpers/index.js'

const BookingModal = ({setOpen, booking,open,rental, setOpenAlert, setBookedOutDates, bookedOutDates }) => {

	const {startAt, endAt, guests, days, total } = booking;

	let title = rental && rental.title
  let id = rental && rental._id
	let dailyRate = rental && rental.dailyRate
  let dateRange = getRangeOfDates(startAt, endAt, 'Y/MM/DD')
  booking.rental = id

	const cancelBooking = e => {
		setOpen(false);

	}

	const confirmBooking = e => {


		fetch(`https://capstone-server-backend.herokuapp.com/bookings`,{
            method: "post",
            body:JSON.stringify(booking),
            headers: {
                "Authorization" : `Bearer ${localStorage['appState']}`,
                'Content-Type' : 'application/json'
            }
        })
        .then(res => {
            if(res.status !== 400){
              console.log(res.errors)
              setOpen(false)
              setOpenAlert({
                  open: true,
                  severity:"error",
                  message:"Cannot book your own rental "
              })
            }
          return res.json()})
        .then( data => {
            if(data.errors){
              setOpenAlert({
                  open: true,
                  severity:"error",
                  message:"Dates already booked. "
              })
            }else {
              setOpen(false)
              setOpenAlert({
                    open: true,
                    severity:"success",
                    message:"Your booking has been confirmed. "
                })
             setBookedOutDates(bookedOutDates=> bookedOutDates.concat(dateRange))

            }
          })

	}

  return (

  	<Modal show={open} onHide={cancelBooking}>
        <Modal.Header closeButton>
          <Modal.Title>Booking Details</Modal.Title>
        </Modal.Header>

        <Modal.Body>
        	<p className="title"> {title} </p>
        	<p className="guests"> For <strong>{guests}</strong> Guests </p>
        	<p className="dates"> From <strong> {startAt} </strong> to <strong> {endAt}</strong> </p>
        	<hr/>
        	<p className="dailyRate"> &#8369; {dailyRate}/night for <strong>{days}</strong> nights</p>
        	<hr/>
        	<p className="total"> Total: &#8369; {total} </p>
        </Modal.Body>
        
        <Modal.Footer>
          <button className="btn btn-danger" onClick={cancelBooking}>Cancel</button>
          <button className="btn btn-primary" onClick={confirmBooking}>Confirm booking</button>
        </Modal.Footer>
     </Modal>
  )
}

export default BookingModal;