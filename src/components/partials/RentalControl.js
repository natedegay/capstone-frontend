import React from 'react';
import { Link } from 'react-router-dom';

const RentalControl = ({id,setDeletedRental}) => {

	const handleClick = () => {
		fetch(`https://capstone-server-backend.herokuapp.com/rentals/${id}`, {
			method: "delete",
			headers: {
				'Authorization' : `Bearer ${localStorage['appState']}`
			}
		})
		.then( res=> res.json())
		.then( data=> {
			console.log(data)
			if(setDeletedRental) {
				setDeletedRental({_id: id})
			}
		})
	}
	return (
	    <>
		    <button  
		    	className="btn btn-danger my-1 w-100"
		    	onClick={handleClick}
		    >
		    	Delete Rental
		    </button>
	    </>
  	)
}

export default RentalControl;