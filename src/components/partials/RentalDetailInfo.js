import React from 'react';

const RentalDetailInfo = ({rental}) => {
  return (
    <div className = 'rental'>
    	<h2 className={`rental-type ${rental.category}`}>{rental.category}</h2>

    	<h1 className='rental-title'>{rental.title}</h1>
    	<h2 className='rental-city'>{rental.city}</h2>
    	<div className='rental-room-info'>
    		<span className="badge badge-info">{rental.bedrooms} bedrooms</span>
    		<span className="badge badge-primary">{rental.bedrooms + 4} guests</span>
    		<span className="badge badge-secondary">{rental.bedrooms +2} beds</span>
    	</div>
    </div>
  )
}

export default RentalDetailInfo;
