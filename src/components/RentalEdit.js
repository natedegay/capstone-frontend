import React, {useState,useEffect} from 'react';
import InputGroup from './partials/InputGroup';
import {useParams} from 'react-router-dom';
import HouseIcon from '@material-ui/icons/House';
import PeopleIcon from '@material-ui/icons/People';
import HotelIcon from '@material-ui/icons/Hotel';
import CreditCardIcon from '@material-ui/icons/CreditCard';

const RentalEdit = (props) => {

	const { id } = useParams();

    const [rental, setRental ] = useState({
        title:"",
		image:"",
		city:"",
		street:"",
		category:"",
		shared:true,
		bedrooms:0,
		description:"",
		dailyRate:0

    });

     const [ isShared, setIsShared ] = useState(false)

     useEffect(()=>{
        fetch(`https://capstone-server-backend.herokuapp.com/rentals/${id}`)
        .then( response => response.json())
        .then( data=> {
        	setRental(data)
        	setIsShared(data.shared)
        })
    },[])


    const handleChange = e => {
        setRental({
            ...rental,
            [e.target.name] : e.target.value
        })
    }

    const handleShared = e => {
    	setIsShared(true)
    	setRental({
    		...rental,
    		shared:true
    	})
    }

    const handleEntire = e => {
    	setIsShared(false)
    	setRental({
    		...rental,
    		shared:false
    	})
    }

    const handleChangeFile = e => {
        setRental({
            ...rental,
            image: e.target.files[0]
        })
    }

    const handleSubmit = e => {
        e.preventDefault()

        let formData = new FormData();
        formData.append('title',rental.title)
		formData.append('city',rental.city)
		formData.append('street',rental.street)
		formData.append('category',rental.category)
		formData.append('shared',rental.shared)
		formData.append('bedrooms',rental.bedrooms)
		formData.append('description',rental.description)
		formData.append('dailyRate',rental.dailyRate)
        
        if(rental.image) {
            formData.append('image',rental.image);

        }

        fetch(`https://capstone-server-backend.herokuapp.com/rentals/${id}`, {
            method: "put",
            headers: {
                'Authorization' : `Bearer ${localStorage['appState']}`
            },
            body: formData
        })
        .then( res=> res.json())
        .then( data => {
            console.log(data)
        })
    }

    useEffect(()=>{
        setRental({
            ...props.rental,
            image: ""
        })
    },[props.rental]);



  return (
    <div className="container">
    	<div className="row">
    		<div className="col-12 col-md-8 col-lg-6 mx-auto">
    			<form onSubmit={handleSubmit}>
		    		<InputGroup
		    			name="title"
		    			displayName="Rental Name: "
		    			type="text"
		            	handleChange={handleChange}
		            	value={rental.title}
		    		/>

		    		<InputGroup
		    			name="image"
		    			displayName="Rental Name: "
		    			type="file"
		            	handleChange={handleChangeFile}
		    		/>

		    		<InputGroup
		    			name="city"
		    			displayName="City: "
		    			type="text"
		            	handleChange={handleChange}
		            	value={rental.city}
		    		/>

		    		<InputGroup
		    			name="street"
		    			displayName="Street: "
		    			type="text"
		            	handleChange={handleChange}
		            	value={rental.street}
		    		/>

		    		<div className="form-group">
		    			<label htmlFor="category">Category</label>
		    			<select 
		    				className="form-control" 
		    				id="category" 
		    				name="category" 
		    				onChange={handleChange}
		    			>
		    				<option selected hidden disabled>{rental.category}</option>
		    				<option value="Treehouses">Treehouses</option>
		    				<option value="Huts">Huts</option>
		    				<option value="Tiny Houses">Tiny Houses</option>
		    				<option value="Barns">Barns</option>
		    			</select>
		    		</div>

		    		<InputGroup
		    			name="shared"
		    			displayName="Shared: "
		    			type="text"
		    			value={true}
		            	handleChange={handleChange}
		            	value={rental.shared}
		            	disabled="disabled"
		    		/>

		    		<div className="form-check form-check-inline">
		    			<input 
		    				className="form-check-input" 
		    				type="radio"
		    				id="shared"
		    				name="shared"
		    				onChange={handleShared}
		    			/>
		    			<label className="form-check-label" htmlFor="shared">Shared</label>
		    		</div>
		    		<div className="form-check form-check-inline">
		  				<input 
		    				className="form-check-input" 
		    				type="radio"
		    				id="entire"
		    				name="shared"
		    				onChange={handleEntire}
		    			/>
		    			<label className="form-check-label" htmlFor="entire">Entire Place</label>
		    		</div>
		    		<hr/>

		    		<InputGroup
		    			name="bedrooms"
		    			displayName="Beds: "
		    			type="number"
		            	handleChange={handleChange}
		            	value={rental.bedrooms}
		    		/>

		    		<label htmlFor="description">Rental Description</label>
		    		<textarea 
		    			className="form-control"
		    			name="description"
		    			id="description"
		    			cols="30"
		    			rows="5"
		            	onChange={handleChange}
		            	value={rental.description}
		    		/>

		    		<InputGroup
		    			name="dailyRate"
		    			displayName="Daily Rate: "
		    			type="number"
		            	handleChange={handleChange}
		            	value={rental.dailyRate}
		    		/>

	    			<button className="btn btn-primary my-3">Edit Rental</button>
	    		</form>
    		</div>
    		<div className="col-12 col-md-8 col-lg-6 mx-auto">
    			<div className="row">
				<div className="col-12 col-md-6">
					<div className="head">
						<h1> {rental.title} </h1>
						<img src={`https://capstone-server-backend.herokuapp.com/${rental.image}`} className="head-img"/>
						<h4> {rental.street}, {rental.city} </h4>
					</div>		
				</div>	
				<div className="col-12 col-md-6">
					<h3> {rental.category} </h3>
					<hr/>
					<div className="description">
						<p>"{rental.description}"</p>
					</div>
				</div>	
			</div>
			<div className="row">
				<div className="col-6">
					<div className="details">
						<hr/>
						{
							!isShared ? [

							<h4><HouseIcon fontSize="large"/> Entire Place</h4>, 
							<h5>You'll have the entire place to yourself.</h5>
							] :
							[<h4><PeopleIcon fontSize="large"/> Shared with Guests</h4>,
							<h5>You'll have awesome company</h5>]

						}
						<hr/>
						<h4><HotelIcon fontSize="large"/> Bedrooms</h4>
						<h5>You'll get {rental.bedrooms} bedrooms for your stay</h5>
						<hr/>
						<h4><CreditCardIcon fontSize="large"/> Rate</h4>
						<h5>Starts at { rental.dailyRate } / night</h5>				
						
						<hr/>
					</div>
				</div>
			</div>
    		</div>
    	</div>
    </div>
  )
}

export default RentalEdit;