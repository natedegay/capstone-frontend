import React from 'react';
import RegisterForm from './partials/RegisterForm';
import './css/Register.css';

const Register = (props) => {
  return (
    <div className="register">
    	<RegisterForm/>
    </div>
  )
}

export default Register;