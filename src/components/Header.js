import React, {useState, useRef, useEffect} from 'react';
import SearchIcon from "@material-ui/icons/Search";
import LanguageIcon from "@material-ui/icons/Language";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import { Avatar, IconButton, MenuList, MenuItem, Button } from "@material-ui/core";
import MenuOpenIcon from '@material-ui/icons/MenuOpen';
import ClickAwayListener from '@material-ui/core/ClickAwayListener';
import Paper from '@material-ui/core/Paper';
import Popper from '@material-ui/core/Popper';
import Grow from '@material-ui/core/Grow';
import { makeStyles } from '@material-ui/core/styles';
import { Link } from "react-router-dom";
import './css/Header.css'

const useStyles = makeStyles((theme) => ({
      root: {
        display: 'flex',
      },
      paper: {
        marginRight: theme.spacing(2),
      },
    }));

const Header = ({authUser}) => {

  const [loggedUser, setLoggedUser ] = useState([]);
  
  useEffect( ()=> {
    setLoggedUser(authUser);
  },[])
    
    const classes = useStyles();
    const [open, setOpen] = useState(false);
    const anchorRef = useRef();



   const handleToggle = () => {
    setOpen((prevOpen) => !prevOpen);
  };

  const handleClose = (event) => {
    if (anchorRef.current && anchorRef.current.contains(event.target)) {
      return;
    }

    setOpen(false);
  };


  return (
    <div className='header'>
        <Link to='/'>
            <img
                className="header__icon"
                src="/camper4.jpg"
                alt=""
            />
        </Link>
        <div className="header__right">
            <div className={classes.root} key="root">

                <IconButton 
                     ref={anchorRef}
                     onClick={handleToggle}
                >
                    <MenuOpenIcon/>
                    {
                        !authUser.isAuth ?
                        <Avatar /> :
                        <Avatar>{loggedUser.fullname.charAt(0)}</Avatar>
                    }
                    
                </IconButton>

                <Popper open={open} anchorEl={anchorRef.current} role={undefined} transition disablePortal>
                    {({ TransitionProps, placement }) => (
                        <Grow
                          {...TransitionProps}
                          style={{ transformOrigin: placement === 'bottom' ? 'center top' : 'center bottom' }}
                        >
                          <Paper>
                            <ClickAwayListener onClickAway={handleClose}>
                              <MenuList autoFocusItem={open} id="menu-list-grow">
                                {
                                    !authUser.isAuth ? 
                                    [
                                        <MenuItem onClick={handleClose}> 
                                            <Link to="/login" style={{ textDecoration: 'none'}}>Login</Link> 
                                        </MenuItem>,

                                        <MenuItem onClick={handleClose}> 
                                            <Link to="/register" style={{ textDecoration: 'none'}}>Register </Link> 
                                        </MenuItem>
                                    ] : 
                                    [
                                        <MenuItem onClick={handleClose}> 
                                            <Link to="/rentals/users/manage" style={{ textDecoration: 'none'}}>Manage Rentals </Link> 
                                        </MenuItem>,                                        
                                        <MenuItem onClick={handleClose}> 
                                            <Link to="/create-rental" style={{ textDecoration: 'none'}}>Create Rental </Link> 
                                        </MenuItem>,
                                        <MenuItem onClick={handleClose}> 
                                            <Link to="/bookings/manage" style={{ textDecoration: 'none'}}>View Bookings </Link> 
                                        </MenuItem>,
                                        <hr/>,
                                        <MenuItem onClick={handleClose}> 
                                            <Link to="/logout" style={{ textDecoration: 'none'}}>Logout </Link> 
                                        </MenuItem>,
                                    ]
                                }

                              </MenuList>
                            </ClickAwayListener>
                          </Paper>
                        </Grow>
                    )}
                </Popper>
            </div>
        </div>
    </div>
  )
}

export default Header;