import React, {useEffect, useState} from 'react';
import ManageCard from './partials/ManageCard';
import {Redirect, Link} from 'react-router-dom';
import './css/RentalManage.css';

const RentalManage = (props) => {
	const [ authUser, setAuthUser ] = useState({
        isAuth: false,
        _id: "",
        fullname:"",
        email:""
    });
    const [ isRedirect, setIsRedirect ] = useState(false);

    const [deletedRental, setDeletedRental] = useState({})

    const [ rentals,setRentals ] = useState([]);

	useEffect(()=>{
        let appState = localStorage["appState"];

        if(appState) {
            fetch("https://capstone-server-backend.herokuapp.com/users/profile",{
                headers: {
                    "Authorization" : `Bearer ${appState}`
                }
            })
            .then( response => response.json())
            .then( data => {
                if(data._id){
                    setAuthUser({
                        isAuth: true,
                        _id: data._id,
                        fullname: data.fullname,
                        email: data.email,
                        isAdmin: data.isAdmin
                    });
                
                }
            })
        }
    },[]);

    useEffect( ()=> {
    	let appState = localStorage["appState"];
  		fetch(`https://capstone-server-backend.herokuapp.com/rentals/users/manage`, {
  			headers: {
                    "Authorization" : `Bearer ${appState}`
                }
  		})
  		.then( response => {return response.json()})
  		.then( rentals => { setRentals(rentals)})
  		console.log(rentals)
  	},[]);

  	useEffect(()=>{
      if(deletedRental){
        setRentals(rentals.filter( rental=> {
          return rental._id !== deletedRental._id
        }))
      }
    },[deletedRental])

    let rentalList = rentals.map( rental => (
  		<div className="col-12 col-md-4" key={rental._id}>
  			<ManageCard  rental={rental} setIsRedirect={setIsRedirect} setDeletedRental={setDeletedRental}  />
  		</div>
		));

    if(rentalList.length < 1) {
    	rentalList = (
    	    <div className="col-12 no-rentals">
    	    	<h3>No rentals to show </h3>
    	    </div>)
    	
    }

  return (
    <div className="container manage">
    	<div className="row">
    		<div className="col-12">
		    	<div className="user text-center">
		    		<h1> Hello, {authUser.fullname}</h1>
		    		<h3> Manage all your rentals here.</h3>
		    		<hr/>
		    	</div>
		    </div>
	    </div>
	    <div className="row title">
	    	<h3> Your Rentals </h3>
	    </div>
	    <div className="row rentals">
	    	{ rentalList}
	    </div>	

    </div>
  )
}

export default RentalManage;