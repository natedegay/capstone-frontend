import React from 'react';
import './css/Footer.css'

const Footer = (props) => {
  return (
    <div className='footer'>
        <p>© 2020 campr! No rights reserved - this is a demo!</p>
        <p>Privacy · Terms · Sitemap · Company Details</p>
    </div>
  )
}

export default Footer;