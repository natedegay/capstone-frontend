import React, {useState, useEffect} from 'react';
import RentalCard from './partials/RentalCard';
import {Redirect, Link} from 'react-router-dom';


const Rental = ({props,errors}) => {

	const [ rentals,setRentals ] = useState([]);

	const [ isRedirect, setIsRedirect ] = useState(false);

	const [deletedRental, setDeletedRental] = useState({})

	useEffect( ()=> {
  		fetch(`https://capstone-server-backend.herokuapp.com/rentals`)
  		.then( response => {return response.json()})
  		.then( rentals => { setRentals(rentals)})
  	},[]);

   useEffect(()=>{
      if(deletedRental){
        setRentals(rentals.filter( rental=> {
          return rental._id !== deletedRental._id
        }))
      }
    },[deletedRental])

  	let rentalList = rentals.map( rental => (
  		<div className="col-12 col-md-4" key={rental._id}>
        <Link to={`/rentals/${rental._id}`} style={{ textDecoration: 'none'}}>
  			 <RentalCard  rental={rental} setIsRedirect={setIsRedirect} setDeletedRental={setDeletedRental}  />
        </Link>
  		</div>
		));

  return (
     <div className="container-fluid">
    	<div className="row">
        	
        		{rentalList}

    	</div>
    </div>
  )
}

export default Rental;