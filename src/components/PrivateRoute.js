import React,{useContext} from 'react';
import { Route, Redirect} from 'react-router-dom';
import { AppContext } form './AppProvider';

const PrivateRoute = (props) => {

	const [authUser] = useContext(AppContext);
  return (
    <div>
    	<Route>
    		{
    			authUser.isAuth ? props.children : <Redirect to ="/404"
    		}
    	</Route>
    </div>
  )
}

export default PrivateRoute;