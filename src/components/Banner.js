import React, { useState } from 'react'
import './css/Banner.css'

const Banner = (props) => {
 
  return (

        <div className="jumbotron banner mt-3">
          <div className="container banner__info">
            <h1 className="display-4">Treat yourself a Break.</h1>
            <h5 className="lead">Discover awesome sites to relax, unwind and enjoy.</h5>
          </div>

        </div>

    
  )
}

export default Banner;