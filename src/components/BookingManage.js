import React, {useEffect, useState, useContext } from 'react';
import * as moment from 'moment';
import './css/BookingManage.css'

const BookingManage = (props) => {

	const [ bookings, setBookings ] = useState([]);

	 useEffect(()=>{
	 	let appState = localStorage["appState"];
       fetch(`https://capstone-server-backend.herokuapp.com/bookings/manage`, {
  			headers: {
                    "Authorization" : `Bearer ${appState}`
                }
  		})
  		.then( response => {return response.json()})
  		.then( bookings => { 
  			setBookings(bookings)
  			console.log(bookings)
  		} )

    },[]);
	 
	 let bookingList = bookings.map( booking => (

	 	<div className="col-12 col-md-6" key={booking._id}>
        	<div className="booked card">
        		<div className="card-header">
        			Booking Code: { booking._id} | Created {moment(booking.createdAt).format('MM-DD-YY')}
        		</div>
        		<div className="card-body">
        			<p className="card-text"> Rental : {booking.rental.title} </p>
        			<hr/>
        			<p className="card-text"> 
        				Rental at <strong>&#8369;{booking.rental.dailyRate}</strong> /night
        				 for <strong>{booking.days}</strong> nights
        			</p>
        			<p className="card-text">
        				Booked from <strong>{moment(booking.startAt).format('MMM-DD-YY')}</strong> to <strong>{moment(booking.endAt).format('MMM-DD-YY')}</strong>
        			</p>
        			<hr/>
        			<p className="card-text">
        				Total: &#8369; {booking.total}
        			</p>

        		</div>
        	</div> 
  		</div>
	 ))

	 if(bookingList.length < 1) {
    	bookingList = (
    	    <div className="col-12 no-rentals">
    	    	<h3>No bookings to show </h3>
    	    </div>)
    	
    }
  return (
  	<div className="container manage">
    	<div className="row">
    		<div className="col-12">
		    	<div className="user text-center">
		    		<h1> Hello</h1>
		    		<h3> View all your bookings here.</h3>
		    		<hr/>
		    	</div>
		    </div>
	    </div>
	    <div className="row title">
	    	<h3> Your Bookings </h3>
	    </div>
	    <div className="row rentals">
	    	{ bookingList}
	    </div>	

    </div>
  )
}

export default BookingManage;