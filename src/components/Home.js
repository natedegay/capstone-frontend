import React, {useState,useEffect} from 'react';
import './css/Home.css';
import Banner from './Banner';
import Card from './Card';
import Rental from './Rental';



const Home = ({props,authUser,message}) => {


  return (

         <div className='home'>
            <Banner authUser={authUser}/>

            <div className='home__section'>
                <Rental />
            </div>

            
        </div>
  )
}

export default Home;