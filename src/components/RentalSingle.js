import React, {useEffect, useState } from 'react';
import {useParams} from 'react-router-dom';
import RentalDetailInfo from './partials/RentalDetailInfo';
import Booking from './Booking';
import './css/RentalSingle.css';
import HouseIcon from '@material-ui/icons/House';
import PeopleIcon from '@material-ui/icons/People';
import HotelIcon from '@material-ui/icons/Hotel';
import CreditCardIcon from '@material-ui/icons/CreditCard';

import {getRangeOfDates} from './../helpers/index.js'


const RentalSingle = ({props,authUser}) => {

	let {id} = useParams();

    const [ rental, setRental ] = useState([]);
    const [ isLoading, setIsLoading ] = useState(true);

    const [ bookedOutDates, setBookedOutDates ] = useState([])

    useEffect(()=>{

        fetch(`https://capstone-server-backend.herokuapp.com/rentals/${id}`)
        .then( response => response.json())
        .then( data => {
            setRental(data)
            setIsLoading(false)
        })
    },[]);


	return (
		<div className="container">
			<div className="row">
				<div className="col-12 col-md-6">
					<div className="head">

						<h1> {rental.title} </h1>
						<img src={`https://capstone-server-backend.herokuapp.com/${rental.image}`} className="head-img"/>
						<h4> {rental.street}, {rental.city} </h4>
					</div>		
				</div>	
				<div className="col-12 col-md-6">
					<h3> {rental.category} </h3>
					<hr/>
					<div className="description">
						<p>"{rental.description}"</p>
					</div>
					<div className="booking">
						<Booking rental={rental} authUser={authUser} />
					</div>
				</div>	
			</div>
			<div className="row">
				<div className="col-6">
					<div className="details">
						<hr/>
						{
							rental.shared ? [

							<h4><HouseIcon fontSize="large"/> Entire Place</h4>, 
							<h5>You'll have the entire place to yourself.</h5>
							] :
							[<h4><PeopleIcon fontSize="large"/> Shared with Guests</h4>,
							<h5>You'll have awesome company</h5>]

						}
						<hr/>
						<h4><HotelIcon fontSize="large"/> Bedrooms</h4>
						<h5>You'll get {rental.bedrooms} bedrooms for your stay</h5>
						<hr/>
						<h4><CreditCardIcon fontSize="large"/> Rate</h4>
						<h5>Starts at { rental.dailyRate } / night</h5>				
						
						<hr/>
					</div>
				</div>
			</div>
		</div>
  	)
}

export default RentalSingle;