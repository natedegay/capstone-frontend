import React, {useState} from 'react';
import InputGroup from './partials/InputGroup';

import Snackbar from '@material-ui/core/Snackbar';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';

import MuiAlert from '@material-ui/lab/Alert';

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}

const RentalCreate = (props) => {

const [open, setOpen] = useState({
    open: false,
    severity:"",
    message:""
  });

    const [rental, setRental ] = useState({
        title:"",
		image:"",
		city:"",
		street:"",
		category:"Treehouses",
		shared:true,
		bedrooms:0,
		description:"",
		dailyRate:0

    });

     const handleClose = (event, reason) => {
      if (reason === 'clickaway') {
        return;
    }

    setOpen({
      open: false
    })
  }

    const handleChange = e => {
        setRental({
            ...rental,
            [e.target.name] : e.target.value
        })
    }

    const handleChangeFile = e => {
        setRental({
            ...rental,
            image: e.target.files[0]
        })
    }

    const handleSubmit = e => {
        e.preventDefault()

        let formData = new FormData();

        formData.append('title',rental.title)
		formData.append('image',rental.image)
		formData.append('city',rental.city)
		formData.append('street',rental.street)
		formData.append('category',rental.category)
		formData.append('shared',rental.shared)
		formData.append('bedrooms',rental.bedrooms)
		formData.append('description',rental.description)
		formData.append('dailyRate',rental.dailyRate)

        fetch(`https://capstone-server-backend.herokuapp.com/rentals`,{
            method: "post",
            body: formData, 
            headers: {
                "Authorization" : `Bearer ${localStorage['appState']}`
            }
        })
        .then(res => {

        	if(res.status === 400)
                {setOpen({
                    open:true,
                    severity:"error",
                    message: "Error creating rental."
                })
            } else {
                setOpen({
                    open:true,
                    severity:"success",
                    message: "Successfully added rental."
                })
            
            }


        	return res.json()

        })
        .then( data => console.log(data))
        
    }
  return (
    <div className="container">
    	<div className="row">
    		<div className="col-12 col-md-8 col-lg-6 mx-auto">

    			<Snackbar 
			        open={open.open} 
			        autoHideDuration={6000} 
			        onClose={handleClose}
			        anchorOrigin={{
			          vertical: 'bottom',
			          horizontal: 'left',
        		}}
      			>
        <Alert onClose={handleClose} severity={open.severity} variant="filled">
          { open.open ? open.message : ""}
        </Alert>
      </Snackbar>
	    		<form onSubmit={handleSubmit}>
		    		<InputGroup
		    			name="title"
		    			displayName="Rental Name: "
		    			type="text"
		            	handleChange={handleChange}
		    		/>

		    		<InputGroup
		    			name="image"
		    			displayName="Rental Image: "
		    			type="file"
		            	handleChange={handleChangeFile}
		    		/>

		    		<InputGroup
		    			name="city"
		    			displayName="City: "
		    			type="text"
		            	handleChange={handleChange}
		    		/>

		    		<InputGroup
		    			name="street"
		    			displayName="Street: "
		    			type="text"
		            	handleChange={handleChange}
		    		/>

		    		<div className="form-group">
		    			<label htmlFor="category">Category</label>
		    			<select 
		    				className="form-control" 
		    				id="category" 
		    				name="category" 
		    				onChange={handleChange}
		    			>
		    				<option selected value="Treehouses">Treehouses</option>
		    				<option value="Huts">Huts</option>
		    				<option value="Tiny Houses">Tiny Houses</option>
		    				<option value="Barns">Barns</option>
		    			</select>
		    		</div>

		    		<div className="form-check form-check-inline">
		    			<input 
		    				className="form-check-input" 
		    				type="radio"
		    				id="shared"
		    				name="shared"
		    				value={true}
		    				onChange={handleChange}
		    			/>
		    			<label className="form-check-label" htmlFor="shared">Shared</label>
		    		</div>
		    		<div className="form-check form-check-inline">
		  				<input 
		    				className="form-check-input" 
		    				type="radio"
		    				id="entire"
		    				name="shared"
		    				value={false}
		    				onChange={handleChange}
		    			/>
		    			<label className="form-check-label" htmlFor="entire">Entire Place</label>
		    		</div>
		    		<hr/>
		    		<InputGroup
		    			name="bedrooms"
		    			displayName="Bedrooms: "
		    			type="number"
		            	handleChange={handleChange}
		    		/>

		    		<label htmlFor="description">Rental Description</label>
		    		<textarea 
		    			className="form-control"
		    			name="description"
		    			id="description"
		    			cols="30"
		    			rows="5"
		            	onChange={handleChange}
		    		/>

		    		<InputGroup
		    			name="dailyRate"
		    			displayName="Daily Rate: "
		    			type="number"
		            	handleChange={handleChange}
		    		/>

	    			<button className="btn btn-primary my-3">Add Rental</button>
	    		</form>
    		</div>
    	</div>
    </div>
  )
}

export default RentalCreate;