import React from 'react';
import {Link, NavLink, Redirect } from 'react-router-dom';

const MainNav = (props) => {
  return (

	  	<nav className="navbar navbar-expand-lg navbar-light bg-light">
		  <Link className="navbar-brand" to="">Rentals</Link>
		  <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
		  	<span className="navbar-toggler-icon"></span>
		  </button>
		  <div className="collapse navbar-collapse" id="navbarNav">

		    <ul className="navbar-nav ml-auto">
		    	<li className="nav-item">
					<NavLink className="nav-link" to="/create-rental">Create Rental</NavLink>
				</li>
				<li className="nav-item">
					<NavLink className="nav-link" to="/login">Login</NavLink>
				</li>
				<li className="nav-item">
				   	<NavLink className="nav-link" to="/register">Register</NavLink>
				</li>
				<li className="nav-item">
				   	<NavLink className="nav-link" to="/logout">Logout</NavLink>
				</li>

		    </ul>
		  </div>
		</nav>
  	)
}


export default MainNav;