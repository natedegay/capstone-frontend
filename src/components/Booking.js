import React, {useState,useEffect,useCallback} from 'react';
import moment from 'moment';
import {getRangeOfDates} from './../helpers/index.js'
import {useParams, Link} from 'react-router-dom';

import "react-date-range/dist/styles.css"; // main style file
import "react-date-range/dist/theme/default.css"; // theme css file
import { DateRangePicker } from "react-date-range";

import { format,add } from 'date-fns';
import BookingModal from './partials/BookingModal';

import Snackbar from '@material-ui/core/Snackbar';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';

import MuiAlert from '@material-ui/lab/Alert';

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}

const Booking = ({rental,authUser}) => {

	const [ proposedBooking, setProposedBooking ] = useState({
		startAt: '',
		endAt: '',
		guests: ''
	})

const [ guests, setGuests ] = useState(2);

const [ bookedOutDates, setBookedOutDates] = useState([])

const [openAlert, setOpenAlert] = useState({
    open: false,
    severity:"",
    message:""
  });

  const handleClose = (event, reason) => {
      if (reason === 'clickaway') {
        return;
    }

    setOpenAlert({
      open: false
    })
  }

  useEffect(()=> {
    if (rental.bookings && rental.bookings.length > 0){
          const dateRange=[]
          if (rental.bookings && rental.bookings.length > 0){
            rental.bookings.forEach(booking => {
              dateRange.push(getRangeOfDates( booking.startAt, booking.endAt, 'Y/MM/DD'))
            })
              
          }
      setBookedOutDates(dateRange.flat())

    }
  },[rental.bookings])

    const [startDate, setStartDate] = useState(new Date());
    const [endDate, setEndDate] = useState(new Date());

    const selectionRange = {
        startDate: startDate,
        endDate: endDate,
        key: "selection",
      };

    function handleSelect(ranges) {
        setStartDate(ranges.selection.startDate);
        setEndDate(ranges.selection.endDate);
    }

    const handleChange = e => {
      setGuests(
        [e.target.name] = e.target.value
        )
    }

  const [open, setOpen] = useState(false);
  const [dateRange, setDateRange] = useState({});
 
  const toggle = () => setOpen(!open);

  const handleBookNow = e =>{
    const format = 'Y/MM/DD'
    const days = getRangeOfDates( moment(startDate), moment(endDate) ).length-1;
    setProposedBooking({
      startAt: moment(startDate).format(format),
      endAt: moment(endDate).format(format),
      guests: guests,
      days: days,
      total: days*rental.dailyRate,
      rental
    })
    setOpen(true)
  }

  return (
    <div className="dates">
    	<hr/>

      <div className='search'>
            <DateRangePicker 
              ranges={[selectionRange]} 
              onChange={handleSelect} 
              disabledDates={bookedOutDates.map( date=> ( new Date(date) ) )}
              minDate={new Date()}
              />

              <div className="form-group">
                <label htmlFor="guests">Guests</label>
                <input min={0} defaultValue={2} type="number" className="form-control" name="guests" onChange={handleChange}/>
                {
                  authUser.isAuth?
                <button className="btn btn-primary btn-lg mt-2" onClick={handleBookNow}> Book Now </button>
                :
                <Link to="/login">
                  <button className="btn btn-primary btn-lg mt-2"> Login to Book </button>
                </Link>
                }

              </div>
            
        </div>
    <>
    <BookingModal bookedOutDates={bookedOutDates} setBookedOutDates={setBookedOutDates} setOpenAlert={setOpenAlert} setOpen={setOpen} open={open} booking={proposedBooking} rental={proposedBooking.rental}/>
     
     <Snackbar 
        open={openAlert.open} 
        autoHideDuration={6000} 
        onClose={handleClose}
        anchorOrigin={{
          vertical: 'top',
          horizontal: 'right',
        }}
      >
        <Alert onClose={handleClose} severity={ openAlert.severity } variant="filled">
          {  openAlert.message }
        </Alert>
      </Snackbar>
    </>
    </div>
  )
}

export default Booking;