import React from 'react';
import LoginForm from './partials/LoginForm';
import './css/Login.css';

const Login = ({setAuthUser,authUser}) => {

  return (
	    <div className="login">
	
	    	<LoginForm setAuthUser={setAuthUser}/>

	    </div>
  )
}

export default Login;